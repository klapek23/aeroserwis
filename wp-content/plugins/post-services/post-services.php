<?php

/*
Plugin Name: Post services
Text Domain: post-services
Domain Path: /languages
Description: Adds services post type.
Version: 1.0
Author: Dawid Wojtyca
*/

define('POST_SERVICES_DIR_URL', plugin_dir_url( __FILE__ ));
define('POST_SERVICES_PATH', plugin_dir_path( __FILE__ ));


//load plugin textdomain
load_plugin_textdomain( 'post-services', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
//require main plugin class
require_once(POST_SERVICES_PATH . 'classes/PostServicesClass.php');
$init = new PostServicesClass();

add_action('init', array($init, 'createPostType'));
?>