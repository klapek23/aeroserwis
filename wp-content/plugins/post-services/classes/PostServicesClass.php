<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-06
 * Time: 18:38
 */


class PostServicesClass {

    //create post type
    public function createPostType() {

        $labels = array(
            'name'               => _x( 'Services', 'post type general name', 'post-services' ),
            'singular_name'      => _x( 'Service', 'post type singular name', 'post-services' ),
            'add_new'            => _x( 'Add New', 'service', 'post-services' ),
            'add_new_item'       => __( 'Add New Service', 'post-services' ),
            'edit_item'          => __( 'Edit service', 'post-services' ),
            'new_item'           => __( 'New service', 'post-services' ),
            'all_items'          => __( 'All services', 'post-services' ),
            'view_item'          => __( 'View service', 'post-services' ),
            'search_items'       => __( 'Search services', 'post-services' ),
            'not_found'          => __( 'No services found', 'post-services' ),
            'not_found_in_trash' => __( 'No services found in the Trash', 'post-services' ),
            'parent_item_colon'  => '',
            'menu_name'          => __('Services', 'post-services')
        );
        $args = array(
            'labels'        => $labels,
            'description'   => __('Services post type', 'post-services'),
            'public'        => true,
            'menu_position' => 23,
            'hierarchical'  => true,
            'supports'      => array( 'title', 'editor', 'tags', 'thumbnail', 'revisions', 'page-attributes' ),
            'has_archive'   => true,
            'capability_type' => 'post',
            'rewrite' => array(
                'slug'=>__('services', 'post-services'),
                'with_front'=> false
            )
        );
        register_post_type( 'services', $args );

        flush_rewrite_rules();
    }
} 