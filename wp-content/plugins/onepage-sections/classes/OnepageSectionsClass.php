<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-06
 * Time: 18:38
 */


class OnepageSectionsClass {

    //create post type
    public function createPostType() {

        $labels = array(
            'name'               => _x( 'Sections', 'onepage-sections' ),
            'singular_name'      => _x( 'Section', 'onepage-sections' ),
            'add_new'            => _x( 'Add New', 'onepage-sections' ),
            'add_new_item'       => __( 'Add New Section', 'onepage-sections' ),
            'edit_item'          => __( 'Edit section', 'onepage-sections' ),
            'new_item'           => __( 'New Section', 'onepage-sections' ),
            'all_items'          => __( 'All Sections', 'onepage-sections' ),
            'view_item'          => __( 'View Section', 'onepage-sections' ),
            'search_items'       => __( 'Search Sections', 'onepage-sections' ),
            'not_found'          => __( 'No sections found', 'onepage-sections' ),
            'not_found_in_trash' => __( 'No sections found in the Trash', 'onepage-sections' ),
            'parent_item_colon'  => '',
            'menu_name'          => __('Sections', 'onepage-sections')
        );
        $args = array(
            'labels'        => $labels,
            'description'   => __('All sections on onepage', 'onepage-sections'),
            'public'        => true,
            'menu_position' => 21,
            'supports'      => array( 'title', 'editor', 'tags' ),
            'has_archive'   => true,
            'taxonomies' => array('category'),
        );
        register_post_type( 'sections', $args );

        flush_rewrite_rules();
    }
} 