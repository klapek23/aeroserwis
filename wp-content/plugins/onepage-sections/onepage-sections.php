<?php

/*
Plugin Name: Onepage sections
Text Domain: onepage-sections
Domain Path: /languages
Description: Adds sections support for onepage site.
Version: 1.0
Author: Dawid Wojtyca
*/


define('ONEPAGE_SECTIONS_DIR_URL', plugin_dir_url( __FILE__ ));
define('ONEPAGE_SECTIONS_PATH', plugin_dir_path( __FILE__ ));


//load plugin textdomain
load_plugin_textdomain( 'onepage-sections', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
//require main plugin class
require_once(ONEPAGE_SECTIONS_PATH . 'classes/OnepageSectionsClass.php');
$init = new OnepageSectionsClass();

add_action('init', array($init, 'createPostType'));

?>