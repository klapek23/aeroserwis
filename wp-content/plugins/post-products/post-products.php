<?php

/*
Plugin Name: Post products
Domain Path: /languages
Description: Adds products post type.
Version: 1.0
Author: Dawid Wojtyca
*/

define('POST_PRODUCTS_DIR_URL', plugin_dir_url( __FILE__ ));
define('POST_PRODUCTS_PATH', plugin_dir_path( __FILE__ ));


//load plugin textdomain
load_plugin_textdomain( 'post-products', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
//require main plugin class
require_once(POST_PRODUCTS_PATH . 'classes/PostProductsClass.php');
$init = new PostProductsClass();

add_action('init', array($init, 'createPostType'));
?>