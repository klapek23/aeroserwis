<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-06
 * Time: 18:38
 */


class PostProductsClass {

    //create post type
    public function createPostType() {

        $labels = array(
            'name'               => _x( 'Products', 'post-products' ),
            'singular_name'      => _x( 'Product', 'post-products' ),
            'add_new'            => _x( 'Add new', 'post-products' ),
            'add_new_item'       => __( 'Add new product', 'post-products' ),
            'edit_item'          => __( 'Edit product', 'post-products' ),
            'new_item'           => __( 'New product', 'post-products' ),
            'all_items'          => __( 'All products', 'post-products' ),
            'view_item'          => __( 'View product', 'post-products' ),
            'search_items'       => __( 'Search products', 'post-products' ),
            'not_found'          => __( 'No products found', 'post-products' ),
            'not_found_in_trash' => __( 'No products found in the Trash', 'post-products' ),
            'parent_item_colon'  => '',
            'menu_name'          => __('Products', 'post-products')
        );
        $args = array(
            'labels'        => $labels,
            'description'   => __('Products post type', 'post-products'),
            'public'        => true,
            'menu_position' => 22,
            'hierarchical'  => true,
            'supports'      => array( 'title', 'editor', 'tags', 'thumbnail', 'revisions', 'page-attributes' ),
            'has_archive'   => true,
            'capability_type' => 'post',
            'rewrite' => array(
                'slug'=>__('products', 'post-products'),
                'with_front'=> false
            )
        );
        register_post_type( 'products', $args );

        flush_rewrite_rules();
    }
} 