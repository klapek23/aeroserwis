<!DOCTYPE html>
<!--[if IE]><html <?php language_attributes(); ?> class="ie"> <![endif]-->
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

<?php
require_once(dirname(__FILE__) . '/libs/config.php');
?>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo get_bloginfo('name'); ?> | <?php the_title(); ?></title>
    <meta name="description" content="<?php echo get_bloginfo('description'); ?> | <?php the_title(); ?>">
	<meta property="og:image" content="<?php echo site_url(). '/wp-content/themes/omnitec/screenshot.png'; ?>" />
    <!--<meta name="author" content="klapek23">-->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-57x57.png'; ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-60x60.png'; ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-72x72.png'; ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-76x76.png'; ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-114x114.png'; ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-120x120.png'; ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-144x144.png'; ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-152x152.png'; ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-180x180.png'; ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri() . '/favicon/android-icon-192x192.png'; ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() . '/favicon/favicon-32x32.png'; ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri() . '/favicon/favicon-96x96.png'; ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() . '/favicon/favicon-16x16.png'; ?>">
    <link rel="manifest" href="<?php echo get_template_directory_uri() . '/favicon/manifest.json'; ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() . '/favicon/ms-icon-144x144.png'; ?>">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

    <?php
    $gaCode = get_option('ga-code');
    if($gaCode) {
        echo stripslashes($gaCode);
    }
    ?>
</head>

<?php $newspage = (get_page_template_slug() == 'templates/news.php' || is_archive() || is_single() ? true : false); ?>
<?php $familyOfficePage = (get_page_template_slug() == 'templates/family_office_advisory.php' ? true : false); ?>
<?php $homepage = (is_front_page() ? true : false); ?>

<?php if($newspage): ?>
    <base href="/news/">
<?php endif; ?>

<body class="<?php echo ($homepage ? 'homepage' : ''); ?> <?php echo ($familyOfficePage ? 'family-office-page' : ''); ?>">

<header id="main-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-sm-offset-0">
                <h1><a href="/" title="<?php echo get_bloginfo('name'); ?>">
                        <!--<img src="<?php /*echo get_option('main-logo-image'); */?>" class="img-responsive" alt="<?php /*the_title(); */?>">-->
                        <img src="<?php echo get_option('page-logo'); ?>" class="img-responsive" alt="<?php the_title(); ?>">
                    </a></h1>
            </div>
            <div class="col-md-9 hidden-sm hidden-xs">
                <?php wp_nav_menu( array(
                        'theme_location'  => 'lang_menu',
                        'menu'            => 'lang-menu',
                        'container'       => 'nav',
                        'container_class' => 'lang-menu pull-right'
                    )
                ); ?>

                <div class="pull-right">
                    <?php wp_nav_menu(array(
                        'menu'            => 'subpage-main-menu',
                        'container'       => '',
                        'menu_class'      => 'header-main-menu',
                        'menu_id'         => 'menu-header-main-menu'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="main-menu" class="hidden-md hidden-lg">
    <button type="button" class="hamburger" id="main-menu-toggle">
        <span></span>
        <span></span>
        <span></span>
    </button>

    <?php wp_nav_menu(array(
        'menu'            => 'main-menu',
        'container'       => '',
        'menu_class'      => ''
    )); ?>

    <?php dynamic_sidebar('sidebar-menu-bottom'); ?>
</div>