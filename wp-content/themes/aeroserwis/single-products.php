<?php get_header('subpage'); ?>

<?php
    //get data
    $products_name_lang = __('products', 'klapek23_framework');
    $products_page = get_page_by_path($products_name_lang);

    $banner = get_field('banner', $products_page->ID);
    $pageID = $post->ID;
    $curUrl = get_permalink();
?>

<div class="main-banner" style="background-image: url(<?php echo $banner['url']; ?>);">
    <!--<div class="content">
    <article>
        <h3><?php /*the_field('main_banner_subtitle'); */?></h3>
        <h2><?php /*the_field('main_banner_title'); */?></h2>
    </article>
</div>-->
</div>

<section class="single-page page-product">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="back">
                    <a href="#" onclick="window.history.back();" title="<?php _e('Wstecz', 'klapek23_framework'); ?>">
                        <i class="fa fa-angle-left"></i>
                        <span><?php _e('Wstecz', 'klapek23_framework'); ?></span>
                    </a>
                </div>
                <article class="page-content" role="main">
                    <header>
                        <h1><?php echo the_title(); ?></h1>
                    </header>
                    <div class="wysiwyg">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </article>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <aside class="sidebar" role="complementary">
                    <h3><?php _e('Other products', 'klapek23_framework'); ?></h3>
                    <ul>
                        <?php $products = new WP_Query(array(
                            'post_type' =>  'products'
                        ));

                        while($products->have_posts()): $products->the_post();
                            if($post->ID != $pageID):
                        ?>
                            <li>
                                <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    <?php the_title(); ?>
                                </a>
                            </li>
                        <?php
                            endif;
                        endwhile;
                        wp_reset_query();
                        ?>
                    </ul>
                </aside>
            </div>
        </div>
	</div>
</section>

<?php get_footer('subpage'); ?>