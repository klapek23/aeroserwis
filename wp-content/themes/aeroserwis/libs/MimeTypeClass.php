<?php
//mime type class

class MimeTypeClass {

    public function custom_upload_mimes( $existing_mimes=array() ) {
        $existing_mimes['zip'] = 'application/zip';
        $existing_mimes['docx'] = 'document/docx';
        $existing_mimes['pptx'] = 'document/pptx';

        return $existing_mimes;
    }
}

?>