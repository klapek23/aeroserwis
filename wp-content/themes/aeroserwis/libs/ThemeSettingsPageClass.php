<?php
//theme settings page class

class ThemeSettingsPageClass {

    //add page to menu
    public function addPageToMenu() {
        add_submenu_page('themes.php', 'Theme settings page', 'Theme settings', 'manage_options', 'theme-settings', array($this, 'theme_settings'));
    }

    //print page
    public function theme_settings() {
        $this->theme_settings_scripts();
        $this->theme_settings_styles();
        $this->print_content();
    }

    //update fields in database
    private function themeoptions_update() {
        update_option('page-logo', $_POST['page-logo']);
        update_option('ga-code', $_POST['ga-code']);
    }

    //add scripts to media uploader
    private function theme_settings_scripts() {
        wp_enqueue_script('jquery');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_register_script('my-upload', get_template_directory_uri() . '/js/libs/my-upload.js', array('jquery','media-upload','thickbox'));
        wp_enqueue_script('my-upload');
    }

    private function theme_settings_styles() {
        wp_enqueue_style('thickbox');
        wp_register_style('custom-admin', get_template_directory_uri() . '/css/css/custom-admin.css');
        wp_enqueue_style('custom-admin');
    }

    //print content
    private function print_content() { ?>
        <?php if (isset($_POST['update_options']) && $_POST['update_options'] == 'true' ) { $this->themeoptions_update(); } ?>
        <div class="wrap">
            <div id="icon-themes" class="icon32"><br /></div>
            <h2>Theme settings</h2>

            <form method="POST" action="">
                <ul>
                    <li class="optionContainer">
                        <h3>Page logo:</h3>
                        <div>
                            <label for="page-logo">Set page logo</label>
                            <img src="<?php echo get_option('page-logo'); ?>" title="Page logo" style="display: block; margin-bottom: 10px;">
                            <input type="text" name="page-logo" id="page-logo" value="<?php echo get_option('page-logo'); ?>" >
                            <button type="button" class="upload-button"  name="page-logo-button" id="page-logo-button" >Wybierz</button>
                        </div>
                    </li>
                    <li class="optionContainer">
                        <h3>Google Analytics code:</h3>
                        <div>
                            <label for="ga-code" style="float: left;">GA code</label>
                            <textarea name="ga-code" id="ga-code" rows="10" cols="60" ><?php echo stripslashes(get_option('ga-code')); ?></textarea>
                        </div>
                    </li>
                    <li>
                        <input type="hidden" name="update_options" value="true" />
                        <button type="submit" class="button" />Zapisz zmiany</button>
                    </li>
                </ul>
            </form>
        </div>
    <?php 
    }
}

?>