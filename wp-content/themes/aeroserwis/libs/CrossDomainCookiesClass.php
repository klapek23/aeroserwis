<?php

class CrossDomainCookiesClass {

    public function init() {

        global $HTTP_COOKIE_VARS, $HTTP_GET_VARS, $sessionid;

        if(isset($HTTP_COOKIE_VARS['PHPSESSID'])) {
            $sessionid = $HTTP_COOKIE_VARS['PHPSESSID'];
        }

        if(isset($HTTP_GET_VARS['sid']) && ($HTTP_COOKIE_VARS['PHPSESSID'] != $HTTP_GET_VARS['sid'])) {
            SetCookie('PHPSESSID', $HTTP_COOKIE_VARS['PHPSESSID'], 0, '/', '');
            $HTTP_COOKIE_VARS['PHPSESSID'] = $HTTP_GET_VARS['sid'];
            $sessionid = $HTTP_GET_VARS['sid'];

            session_destroy();
            session_id($sessionid);
            session_start();
        }
    }
}

?>