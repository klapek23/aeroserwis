<?php
    require( dirname(__FILE__) . '/../../../../wp-load.php');
	
	//get messages
	$messages = $_POST['messages'];

    //set config
    $requiredFields = array('name', 'email', 'message');
    $emailFields = array('email');
    $requiredMessage = (isset($messages['required']) ? $messages['required'] : 'Pole wymagane');
    $emailMessage = (isset($messages['email']) ? $messages['email']: 'Podaj poprawny e-mail');
    $errors = null;
    
    $smtp_settings = array(
        'host' => get_option('mailserver_url'),
        'username' => get_option('mailserver_login'),
        'password' => get_option('mailserver_pass'),
        'port' => get_option('mailserver_port'),
        'from' => get_option('blogname')
    );
	
    //get data
    $formData = $_POST;
	
	//get errors
	foreach($formData as $name => $value):
		//check required
		if(in_array($name, $requiredFields)):
			if($value == NULL || $value == ''):
				$errors[$name][] = $requiredMessage;
			endif;
		endif;
		//check emails
		if(in_array($name, $emailFields)):
			if(!filter_var($value, FILTER_VALIDATE_EMAIL)):
				$errors[$name][] = $emailMessage;
			endif;
		endif;
	endforeach;
	
	//set form status
	if($errors != null):
		$status = false;
	else:
		/*require 'php-mailer/class.phpmailer.php';

		$mail = new PHPMailer;

		$mail->SetLanguage("en", 'php-mailer/language/');

		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
		                                           // 1 = errors and messages
		                                           // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mail->Host       = $smtp_settings['host'];      // sets GMAIL as the SMTP server
		$mail->Port       = $smtp_settings['port'];                   // set the SMTP port for the GMAIL server
		$mail->Username   = $smtp_settings['username'];  // GMAIL username
		$mail->Password   = $smtp_settings['password'];            // GMAIL password
		$mail->CharSet = 'UTF-8';
		
		$mail->SetFrom($smtp_settings['username'], 'Omnitec - kontakt');
		$mail->Subject    = __('Aeroserwis - kontakt', 'klapek23_framework');
		
		$mail->MsgHTML($body);
		$mail->AddAddress($smtp_settings['username'], "Administrator");

		if(!$mail->send()) {
		    $status = false;
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		    $status = true;
		}*/

        $headers = 'Content-Type: text/html; charset=UTF-8';
        $mailto  = get_bloginfo('admin_email');
        $subject = __('Aeroserwis - kontakt', 'klapek23_framework');

        $body    = '
			<h3 style="color: #000;">' . __('Wiadomośc wysłana przez formularz kontaktowy', 'klapek23_framework') . '</h3>
			<table style="border: 1px solid #000; border-collapse: collapse;">
				<tr>
					<td style="padding: 5px; border: 1px solid #000;">' . __('Imię i nazwisko', 'klapek23_framework') . '</td>
					<td style="padding: 5px; border: 1px solid #000;">'.$formData["name"].'</td>
				</tr>
				<tr>
					<td style="padding: 5px; border: 1px solid #000;">' . __('E-mail', 'klapek23_framework') . '</td>
					<td style="padding: 5px; border: 1px solid #000;">'.$formData["email"].'</td>
				</tr>
				<tr>
					<td style="padding: 5px; border: 1px solid #000;">' . __('Message', 'klapek23_framework') . '</td>
					<td style="padding: 5px; border: 1px solid #000;">'.$formData["message"].'</td>
				</tr>
			</table>
		';

        if(mail($mailto, $subject, $body, $headers)) {
            $status = true;
        } else {
            $status = false;
        }
	endif;
	
	$return = array(
		'status' => $status,
		'errors' => $errors
	);
	
	echo json_encode($return);
	die();
?>