<?php

class TilesClass {
    public function ajaxLoadTiles() {
        global $sitepress;

        $lang = $_POST['lang'];
        if($lang === 'pl_PL') {
            $blogID = 1;
        } else {
            $blogID = 2;
        }

        switch_to_blog($blogID);

        $postType = $_POST['post_type'];
        $startFrom = $_POST['start_from'];
        $posts = get_posts(array(
            'post_type'         => $postType,
            'posts_per_page'    => 6,
            'offset'            => $startFrom
        ));

        foreach($posts as $i => $post) {
            $posts[$i]->pretty_url = get_permalink($post->ID);
            $posts[$i]->image      = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            $posts[$i]->short_info = get_field('short_info', $post->ID);
        }

        restore_current_blog();

        echo json_encode($posts);
        exit;
    }
}