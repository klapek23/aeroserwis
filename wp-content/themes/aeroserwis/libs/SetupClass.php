<?php
//remove admin pages

class SetupClass {

    //make redirections available
    public function do_output_buffer() {
        ob_start();
    }

    //load plugin textdomain (available for translations)
    public function loadTextdomain() {
        load_theme_textdomain( 'klapek23_framework', get_template_directory() . '/languages' );
        $lang = get_option('WPLANG');
        if(!empty($lang)):
            if($lang == 'pl_PL') {
                $locale = 'polish';
            } elseif($lang == 'en_US' || $lang == 'en_EN') {
                $locale = 'english';
            }

            setlocale(LC_ALL, $locale);
        endif;
    }

    //register sidebars
    public function registerSidebars() {
        register_sidebar(
            array(
                'name'          => __( 'Footer logo', 'omnitec' ),
                'id'            => 'footer-logo',
                'class'         => 'col-md-12',
                'before_widget' => '',
                'after_widget'  => '',
                'before_title'  => '',
                'after_title'   => ''
            )
        );
        register_sidebar(
            array(
                'name'          => __( 'Footer address', 'omnitec' ),
                'id'            => 'footer-address',
                'class'         => '',
                'before_widget' => '<address class="addrress">',
                'after_widget'  => '</address>',
                'before_title'  => '',
                'after_title'   => ''
            )
        );
        register_sidebar(
            array(
                'name'          => __( 'Footer contact', 'omnitec' ),
                'id'            => 'footer-contact',
                'class'         => '',
                'before_widget' => '<address class="remote-contact">',
                'after_widget'  => '</address>',
                'before_title'  => '',
                'after_title'   => ''
            )
        );
        register_sidebar(
            array(
                'name'          => __( 'Footer copy', 'omnitec' ),
                'id'            => 'footer-cop',
                'class'         => '',
                'before_widget' => '<div class="copy">',
                'after_widget'  => '</div>',
                'before_title'  => '',
                'after_title'   => ''
            )
        );
		
		register_sidebar(
            array(
                'name'          => __( 'Footer social', 'omnitec' ),
                'id'            => 'footer-social',
                'class'         => '',
                'before_widget' => '<div class="social">',
                'after_widget'  => '</div>',
                'before_title'  => '',
                'after_title'   => ''
            )
        );
    }

    //add thumbnails support
    public function addThumbnails() {
        add_theme_support( 'post-thumbnails' );
    }

    //register menus
    public function registerMenus() {
        register_nav_menus( array(
            'main_menu' => __( 'Main menu', 'klapek23_framework' ),
            'lang_menu' => __( 'Lang menu', 'klapek23_framework' ),
            'subpage_main_menu' => __( 'Subpage main menu', 'klapek23_framework' ),
            'services_menu' => __( 'Services', 'klapek23_framework' )
        ));
    }

    //add post formats support
    public function addPostFormat() {
        add_theme_support( 'post-formats', array('aside', 'image', 'video', 'quote', 'link' ));
    }

    //add HTML5 support for search form
    public function addHTML5() {
        //add_theme_support( 'html5', array( 'search-form') );
    }
        
    //add support for do shortcodes in text widget
    public function widgetDoShortcodes() {
        add_filter('widget_text', 'do_shortcode');
    }

    public function addImagesSizes() {
        add_image_size( 'post-thumbnail', '150', '100', true);
    }

    public function disableAdminBar() {
        show_admin_bar(false);
    }

    //enqueue additional scripts and styles
    function register_styles() {
        wp_deregister_script( 'jquery' );
        wp_register_style('style', get_template_directory_uri() . (getenv('APPLICATION_ENV') == 'dev' ? '/css/build/dev/style.css' : '/css/build/prod/style.css') . '"' );

        wp_enqueue_style('style');
    }

    function register_scripts() {
        wp_deregister_script( 'jquery' );
        wp_register_script('gmaps-api', 'http://maps.googleapis.com/maps/api/js?key=AIzaSyC6yqIikzFJkjJDT9VyOliI8Nb8mLNQW7U' );

        wp_register_script('scripts', get_template_directory_uri() . (getenv('APPLICATION_ENV') == 'dev' ? '/js/build/dev/scripts.js' : '/js/build/prod/scripts.js') . '"');

        wp_enqueue_script('gmaps-api');
        wp_enqueue_script('scripts');
    }

    //render widgets titles as html
    public function html_widget_title( $title ) {
        $title = str_replace( '[', '<', $title );
        $title = str_replace( '[/', '</', $title );

        $title = str_replace( 's]', 'strong>', $title );
        $title = str_replace( 'e]', 'em>', $title );
        $title = str_replace('br]', 'br>', $title);

        return $title;
    }

}

?>