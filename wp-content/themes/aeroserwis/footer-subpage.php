<footer id="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php wp_nav_menu(array(
                    'menu'            => 'subpage-main-menu',
                    'container'       => 'nav',
                    'container_class' => 'footer-nav',
                    'menu_class'      => 'footer-main-menu',
                    'menu_id'         => 'menu-footer-main-menu'
                )); ?>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
