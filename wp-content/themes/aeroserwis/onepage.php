<?php
/*----------
Template name: One page
---------- */
$sections_template_dir = 'sections-templates/';

//get sections
$sections = get_field('sections');

//get section target if is set
$sectionTarget = (isset($_GET['section']) && is_string($_GET['section']) ? $_GET['section'] : null );
?>

<?php get_header(); ?>

<?php
if($sectionTarget != null) { ?>
<script>
    $(window).load( function() {
        $.scrollTo('<?php echo "#".$sectionTarget; ?>', 1000, { offset:{top: -$('header.main').innerHeight()}});
    });
</script>
<?php } ?>

    <?php
    //loop for displaying sections
    foreach($sections as $section) {
        //transform $section to from WP_Post to classic object
        foreach($section as $sec):
            $section = $sec;
        endforeach;

        //get section category
        $terms = get_the_category($section->ID);
        $terms = json_decode(json_encode($terms[0]));
        $section->cat = $terms->category_nicename;

        //require section template
        require($sections_template_dir . $section->cat . '.php');
    }
    ?>

<?php get_footer(); ?>
