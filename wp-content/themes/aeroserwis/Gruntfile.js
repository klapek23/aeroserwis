module.exports = function(grunt) {

  grunt.initConfig({
    less: {
		dev: {
			options: {
				compress: false,
				sourceMap: true,
				sourceMapURL: '/css/build/dev/style.css.map',
				sourceMapRootpath: '/'
			},
			files: {
				'css/build/dev/style.css': 'css/less/style.less',
			}
		},
		prod: {
			options: {
				compress: true,
				sourceMap: true,
				sourceMapURL: '/css/build/prod/style.css.map',
				sourceMapRootpath: '/'
			},  
			files: {
				'css/build/prod/style.css': 'css/less/style.less',
			}
		}
	},
	concat: {
        js: {
            options: {
                sourceMap: true
            },
            files: {
                'js/build/dev/scripts.js': ['js/libs/modernizr-2.8.2.min.js', 'js/libs/jquery-1.11.1.min.js', 'js/libs/**/*.js', 'js/init.js', 'js/body-animations.js', '!js/build/**/*.*']
            }
        }
	},
	uglify: {
		prod: {
			options: {
				sourceMap: true,
                mangle: false
			},
			files: {
				'js/build/prod/scripts.js': ['js/build/dev/scripts.js']
			}
		}
	},
    webfont: {
      icons: {
        src: 'files/icons/*.svg',
        dest: 'css/fonts/',
        destCss: 'css/libs/',
          options: {
            font: 'icons',
            stylesheet: 'less',
            relativeFontPath: '/css/fonts/',
            types: ['eot', 'woff', 'ttf', 'svg'],
            engine: 'node'
        }
      }
    },
	watch: {
		css: {
			files: ['css/less/**/*.*'],
			tasks: ['less', 'watch']
		},
		js_prod: {
			files: ['js/libs/modernizr-2.8.2.min.js', 'js/libs/jquery-1.11.1.min.js', 'js/libs/angular.min.js', 'js/**/*.js', '!js/build/**/*.*'],
			tasks: ['concat', 'uglify', 'watch_prod']
		},
        js_dev: {
            files: ['js/**/*.js', '!js/build/**/*.*'],
            tasks: ['concat', 'watch_dev']
        },
        icons: {
            files: ['files/icons/*.svg'],
            tasks: ['webfont', 'watch_icons']
        }
	}
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-webfont');

  grunt.registerTask('prod', ['less', 'concat', 'uglify']);
  grunt.registerTask('dev', ['less', 'concat']);

  grunt.registerTask('watch_prod', ['watch']);
  grunt.registerTask('watch_dev', ['watch:css', 'watch:js_dev']);
  grunt.registerTask('watch_icons', ['watch:icons']);

  grunt.registerTask('create_icons', ['webfont']);

};