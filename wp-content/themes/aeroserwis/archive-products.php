<?php

get_header('subpage');

//get data
$products_name_lang = __('products', 'klapek23_framework');
$products_page = get_page_by_path($products_name_lang);

$banner = get_field('banner', $products_page->ID);
?>

<div class="main-banner" style="background-image: url(<?php echo $banner['url']; ?>);">
    <!--<div class="content">
        <article>
            <h3><?php /*the_field('main_banner_subtitle'); */?></h3>
            <h2><?php /*the_field('main_banner_title'); */?></h2>
        </article>
    </div>-->
</div>

<section class="products" id="products">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $products_page->post_title; ?></h2>
            </div>
        </div>
        <div class="row">
            <?php
            $productID = 1;

            $query = new WP_Query(array(
                'post_type'         => 'products',
                'posts_per_page'    => 6
            ));

            while($query->have_posts()): $query->the_post();
                $thumbUrl = wp_get_attachment_url(get_post_thumbnail_id());
                $intro = get_field('short_info', $post->ID);
                ?>
                <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 product">
                    <article>
                        <a href="<?php echo get_permalink(($post->ID)); ?>" title="<?php the_title(); ?>">
                            <div class="flip">
                                <div class="card">
                                    <div class="front">
                                        <img src="<?php echo $thumbUrl; ?>" class="img-responsive" alt="<?php the_title(); ?>">
                                    </div>
                                    <div class="back">
                                        <h4><?php the_title(); ?></h4>
                                        <p><?php echo $intro; ?></p>
                                    </div>
                                </div>
                            </div>
                            <h4><?php the_title(); ?></h4>
                        </a>
                    </article>
                </div>

                <?php if($productID % 3 == 0): ?>
                    <div class="clearfix hidden-xs hidden-sm"></div>
                <?php endif; ?>

                <?php if($productID % 2 == 0): ?>
                    <div class="clearfix hidden-md hidden-lg"></div>
                <?php endif; ?>
            <?php
            $productID++;
            endwhile;
            ?>
        </div>

        <div class="load-more-items" data-lang="<?php echo get_locale(); ?>">
            <div class="center">
                <span><?php _e('Load more products', 'aeroserwis.localhost/'); ?></span>
                <i class="fa fa-angle-down"></i>
                <i class="fa fa-spinner spinner-icon"></i>
            </div>
        </div>
    </div>
</section>

<?php get_footer('subpage'); ?>