<?php get_header('subpage'); ?>

<?php
//get data
$products_name_lang = __('services', 'klapek23_framework');
$products_page = get_page_by_path($products_name_lang);
$banner = get_field('banner', $products_page->ID);
$pageID = $post->ID;
$curUrl = get_permalink();
?>

    <div class="main-banner" style="background-image: url(<?php echo $banner['url']; ?>);">
        <!--<div class="content">
    <article>
        <h3><?php /*the_field('main_banner_subtitle'); */?></h3>
        <h2><?php /*the_field('main_banner_title'); */?></h2>
    </article>
</div>-->
    </div>

    <section class="single-page page-product">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="back">
                        <a href="#" onclick="window.history.back();" title="<?php _e('Wstecz', 'klapek23_framework'); ?>">
                            <i class="fa fa-angle-left"></i>
                            <span><?php _e('Wstecz', 'klapek23_framework'); ?></span>
                        </a>
                    </div>
                    <article class="page-content" role="main">
                        <header>
                            <h1><?php echo the_title(); ?></h1>
                        </header>
                        <div class="wysiwyg">
                            <?php echo apply_filters('the_content', $post->post_content); ?>
                        </div>
                    </article>
                </div>
                <div class="col-md-3 col-md-offset-1">
                    <aside class="sidebar" role="complementary">
                        <h3><?php _e('Other services', 'klapek23_framework'); ?></h3>
                        <ul>
                            <?php $services = new WP_Query(array(
                                'post_type' =>  'services'
                            ));

                            while($services->have_posts()): $services->the_post();
                                if($post->ID != $pageID):
                                    ?>
                                    <li>
                                        <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
                                            <i class="fa fa-angle-right"></i>
                                            <?php the_title(); ?>
                                        </a>
                                    </li>
                                <?php
                                endif;
                            endwhile;
                            wp_reset_query();
                            ?>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </section>

    <?php
    //get data
    $hasMap = get_field('has_map', $post->ID);
    if($hasMap):
        $mapWindowTitle = get_field('map_window_title', $post->ID);
        $list = get_field('contact_items', $post->ID);
        $marker = get_field('marker', $post->ID);
        $markerTitle = get_field('marker_title', $post->ID);
        $markerInfowindowContent = get_field('marker_infowindow_content', $post->ID);
        $mapCenter = get_field('map_center', $post->ID);
        ?>

        <section class="map" id="map">
            <div id="gmap"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-7 col-xxs-10">
                        <article class="contactInfo">
                            <h3><?php echo $mapWindowTitle; ?></h3>
                            <div class="table">
                                <?php foreach($list as $row): ?>
                                    <div class="table-row">
                                        <div class="cell"><?php echo $row['text']; ?></div>
                                        <div class="cell ico">
                                            <?php echo $row['icon']; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <script>
            //google map
            (function($) {
                var $gmap = $('#gmap');
                if($gmap.length > 0) {
                    function mapInit() {
                        var styles = [
                            {
                                "featureType": "landscape",
                                "elementType": "geometry",
                                "stylers": [
                                    { "color": "#e8e7e3" }
                                ]
                            },{
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    { "color": "#eef1ed" }
                                ]
                            },{
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                    { "color": "#ffffff" }
                                ]
                            },{
                                "featureType": "all",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    { "color": "#9f9f9f" }
                                ]
                            },{
                                "featureType": "all",
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    { "color": "#ffffff" }
                                ]
                            },{
                                "featureType": "road",
                                "elementType": "labels.icon",
                                "stylers": [
                                    { "visibility": "off" }
                                ]
                            },{
                                "featureType": "poi",
                                "elementType": "labels.icon",
                                "stylers": [
                                    { "visibility": "off" }
                                ]
                            },{
                                "featureType": "transit",
                                "elementType": "labels.icon",
                                "stylers": [
                                    { "visibility": "off" }
                                ]
                            },{
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    { "color": "#cbdffc" }
                                ]
                            }
                        ]

                        var styledMap = new google.maps.StyledMapType(styles,
                            {name: "Styled Map"});

                        var mapProp = {
                            center: new google.maps.LatLng(<?php echo $mapCenter; ?>),
                            zoom: 13,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            panControl: false,
                            zoomControl: true,
                            mapTypeControl: false,
                            scaleControl: false,
                            streetViewControl: false,
                            overviewMapControl: false,
                            scrollwheel: false
                        };
                        var map = new google.maps.Map(document.getElementById("gmap"), mapProp);

                        map.mapTypes.set('map_style', styledMap);
                        map.setMapTypeId('map_style');

                        var infowindow = new google.maps.InfoWindow({
                            content: '<?php echo trim(preg_replace('/\s\s+/', ' ', nl2br($markerInfowindowContent))); ?>'
                        });

                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(<?php echo $marker; ?>),
                            map: map,
                            title: '<?php echo trim(preg_replace('/\s\s+/', ' ', nl2br($markerTitle))); ?>',
                            icon: '/wp-content/themes/aeroserwis/img/ico/map-marker.png'
                        });

                        marker.addListener('click', function() {
                            infowindow.open(map, marker);
                        });
                    }
                    google.maps.event.addDomListener(window, 'load', mapInit);
                }
            })(jQuery);
        </script>
    <?php
        endif;
    ?>

<?php get_footer('subpage'); ?>