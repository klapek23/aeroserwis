$(window).load( function() {
    var touch = Modernizr.touch;
    if(!touch) {
        animateBody();
    } else {
        staticBody();
    }
});

//body animations
function animateBody() {
    var $services = $('.services').find('article');
    var $products = $('.products').find('article');

    if($services.length > 0) {
        animateServices($services);
    }

    if($products.length > 0) {
        animateProducts($products);
    }


    //animate services
    function animateServices(services) {
        $(document).ready(function() {
            var $services = $(services),
                $box      = $(services).closest('section'),
                speed     = 300;


            //services fade animations
            if($services.length > 0 && isScrolledIntoView($box)) {
                servicesFadeIn($services, speed);
            }
        });

        $(window).scroll(function () {
            var $services = $(services),
                $box      = $(services).closest('section'),
                speed     = 300;

            if($services.length > 0 && isScrolledIntoView($box)) {
                servicesFadeIn($services, speed);
            }
        });

        function servicesFadeIn($services, delay) {
            $.each($services, function(i, el) {
                $(el).delay(i * delay).queue(function(next){
                    $(el).addClass('fadedIn');
                    next();
                });
            });
        }
    }


    //animate products
    function animateProducts(products) {
        $(document).ready(function() {
            var $products = $(products),
                $box      = $(products).closest('section'),
                speed     = 300;


            //services fade animations
            if($products.length > 0 && isScrolledIntoView($box)) {
                servicesFadeIn($products, speed);
            }
        });

        $(window).scroll(function () {
            var $products = $(products),
                $box      = $(products).closest('section'),
                speed     = 300;

            if($products.length > 0 && isScrolledIntoView($box)) {
                servicesFadeIn($products, speed);
            }
        });

        function servicesFadeIn($services, delay) {
            $.each($services, function(i, el) {
                $(el).delay(i * delay).queue(function(next){
                    $(el).addClass('fadedIn');
                    next();
                });
            });
        }
    }


    //check if element is visible
    function isScrolledIntoView(elem) {
        var $elem = $(elem);
        var $window = $(window);

        var docViewTop = $window.scrollTop();
        var docViewBottom = docViewTop + $window.height();

        var elemTop = $elem.offset().top;
        var elemBottom = elemTop + $elem.height();

        return ((elemTop <= docViewBottom));
    }
};

function staticBody() {
    var $services = $('.services').find('article');
    var $products = $('.products').find('article');

    $services.addClass('fadedIn');
    $products.addClass('fadedIn');
}