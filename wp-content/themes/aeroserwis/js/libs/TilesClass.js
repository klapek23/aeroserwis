var TilesClass = function(container, itemClass, button, postType) {
    this.$container             =   $(container);
    this.itemClass              =   itemClass;
    this.postType               =   postType;
    this.items                  =   this.$container.find(itemClass);
    this.defaultVisibleElements =   6;
    this.countVisibleElements   =   0;
    this.buttonVisible          =   true;
    this.$button                =   this.$container.find(button);
    this.lang                   =   'pl_PL';

    //set items visibility
    if(this.countAllElements > this.countVisibleElements) {
        $.each(this.items, function(i, el) {
            if(i + 1 > this.defaultVisibleElements) {
                var $el = $(el);
                $el.fadeOut();
                $el.addClass('fadedOut');
                this.countVisibleElements = this.countVisible();
            }
        }.bind(this));
    }

    this.$button.on('click', function(e) {
        e.stopPropagation();

        this.lang = $(e.currentTarget).attr('data-lang');
        this.loadMore();
    }.bind(this));
};

TilesClass.prototype = {
    constructor: TilesClass,
    countVisible: function() {
        return this.$container.find(this.itemClass + ':not(.fadedOut)').length;
    },
    loadMore: function() {
        this.$button.addClass('loading');

        var request = $.post('/wp-admin/admin-ajax.php', {
            action: 'load_items',
            post_type: this.postType,
            lang: this.lang,
            start_from: this.countVisible()
        })
        .done(function(data) {
            var posts = JSON.parse(data);

            if(posts.length < 6) {
                this.hideButton();
            }

            $.each(posts, function(i, o) {
                var newPost = '<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 product fadedOut">' +
                    '<article>' +
                    '<a href="'+ o.pretty_url +'" title="'+ o.post_title +'">' +
                    '<div class="flip">' +
                    '<div class="card">' +
                    '<div class="front">' +
                    '<img src="'+ o.image +'" class="img-responsive" alt="'+ o.post_title +'">' +
                    '</div>' +
                    '<div class="back">' +
                    '<h4>'+ o.post_title +'</h4>' +
                    '<p>'+ o.short_info +'</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<h4>'+ o.post_title +'</h4>' +
                    '</a>' +
                    '</article>' +
                    '</div>';

                if((i+1) % 3 == 0) {
                    newPost += '<div class="clearfix hidden-xs hidden-sm"></div>';
                } else if((i+1) % 2 == 0) {
                    newPost += '<div class="clearfix hidden-md hidden-lg"></div>';
                }

                var $newPost = $(newPost);

                $newPost.appendTo(this.$container.find(this.itemClass).parent());

                this.$container.find(this.itemClass + '.fadedOut').fadeIn(1000, function() {
                    $(this).removeClass('fadedOut').children('article').addClass('fadedIn');
                });

                if(i + 1 >= posts.length) {
                    this.$button.removeClass('loading');
                }
            }.bind(this));
        }.bind(this))
        .fail( function(error) {
            console.log('error: ' + error.getErrorMessages());
        });
    },
    hideButton: function() {
        this.$button.fadeOut(1000, function() {
            this.$button.addClass('fadedOut');
            this.buttonVisible = !this.buttonVisible;
        }.bind(this));
    }
};