$(document).ready(function() {

    $('select').select2({
        allowClear: true
    });

    //fancybox init
    $('[rel="fancybox"]').fancybox();

    //placeholders
    $('input[placeholder], textarea[placeholder]').placeholderEnhanced();

    //main menu toggle
    $('#main-menu-toggle').on('click', function(e) {
        toggleMainMenu(e.currentTarget);
    });

    $(document).on( 'keydown', function (e) {
        if(e.keyCode == 27 && $('#main-menu').hasClass('active')) {
            toggleMainMenu($('#main-menu-toggle'));
        }
    });

    function toggleMainMenu(button) {
        $(button).toggleClass('active');
        $('#main-menu').toggleClass('active');
        $('body').toggleClass('blocked');
    };


    //menu page scrolling
    $('ul.header-main-menu li a, ul#menu-main-menu li a, .footer-nav ul li a').on('click', function(e) {
        var id = $(this).attr('href');
        $.scrollTo($(id).offset().top - $('header#main-header').outerHeight(), 800);
        e.preventDefault();
    });


    //sticky header
    stickyHeader();

    $(window).scroll( function() {
        stickyHeader();
    });

    function stickyHeader() {
        var offsetTop = $(window).scrollTop();
        if(offsetTop > 0) {
            $('#main-header').addClass('sticky');
            $('#main-menu').addClass('sticky');
        } else {
            $('#main-header').removeClass('sticky');
            $('#main-menu').removeClass('sticky');
        }
    }


    //init main slider
    if($('.main-slider').length > 0) {
        var mainSlider = new sliderClass('.main-slider', {autoHeight: false, slideToContent: false, autoPlay: true, interval: 14000});
        mainSlider.init();
    }

    //init tabs slider
    /*if($('.tabs-module').length > 0) {
        var tabsSlider = new sliderClass('.tabs-module', {autoHeight: true, slideToContent: false}, animateNavClass);
        tabsSlider.init();
    }*/

    //accordion module
    /*if($('.accordion-module').length > 0) {
        var accordionModule = new accordionClass('.accordion-module');
        accordionModule.init();
    }*/

    //products page
    (function($) {
        if($('.products').length > 0) {
            var productsTiles = new TilesClass('.products', '.product', '.load-more-items', 'products');
        }
    })(jQuery);

    //references slider
    (function($) {
        var $referencesSlider = $('.references-slider');
        if($referencesSlider.length > 0) {
            $referencesSlider.owlCarousel({
                items: 2,
                autoPlay: 8000,
                itemsDesktop : [980,2],
                itemsDesktopSmall : [980,2],
                itemsTablet: [768,1]
            });
        }
    })(jQuery);

    //news slider
    (function($) {
        var $newsSlider = $('.news-slider');
        if($newsSlider.length > 0) {
            $newsSlider.owlCarousel({
                items: 3,
                slideSpeed: 500,
                autoPlay: true,
                pagination: false,
                navigation: true,
                itemsDesktop : [980,2],
                itemsDesktopSmall : [980,2],
                itemsTablet: [768,1]
            });
        }
    })(jQuery);

    $('.cookies').cookieLaw();

    (function() {
        function setBodyPadding() {
            $('body').css({paddingBottom: $('footer#main-footer').outerHeight()});
        };

        setBodyPadding();

        $(window).on('resize', function() {
            setBodyPadding();
        });
    })();

    //$.scrollSpeed(100, 800);

    $("html").niceScroll({
        scrollspeed: 80,
        mousescrollstep: 80,
        autohidemode: false,
        horizrailenabled: false,
        cursoropacitymin:.8,
        cursorwidth: 8,
        cursorcolor: '#4F565E',
        cursorborder: 'none',
        cursorborderradius: '0px',
        background: 'rgba(255,255,255,.4)',
        railoffset: 150,
        zindex: 2000
    });
});

$(window).load(function() {

    $('body').addClass('faded');

});



//javascript cookies
(function($) {
    $.fn.cookieLaw = function( options ) {

        //defaults
        var settings = $.extend({
            closeButton: '.close-button a',
            expires: 365
        }, options );

        var obj = this;

        //initialize plugin
        init(obj);

        //initialize plugin
        function init(obj) {
            var el = {
                $container: obj,
                $closeButton: settings.closeButton
            };
            start(el.$container, el.$closeButton);
        }

        //start
        function start(container, closeButton) {
            $(container).on('click', closeButton, function(e) {
                $.cookie("cookies-accept", 1, {expires: settings.expires});
                $(container).animate({opacity: 0}, { speed: 200, complete: function() {
                    $(container).slideUp();
                }});

                e.preventDefault();
            });
        }

        return this;
    };
})(jQuery);