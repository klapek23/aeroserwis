<?php

get_header('subpage');

//get data
$products_name_lang = __('services', 'klapek23_framework');
$products_page = get_page_by_path($products_name_lang);

$banner = get_field('banner', $products_page->ID);
?>

<div class="main-banner" style="background-image: url(<?php echo $banner['url']; ?>);">
    <!--<div class="content">
        <article>
            <h3><?php /*the_field('main_banner_subtitle'); */?></h3>
            <h2><?php /*the_field('main_banner_title'); */?></h2>
        </article>
    </div>-->
</div>

<?php get_footer('subpage'); ?>