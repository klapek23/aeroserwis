<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-04
 * Time: 18:51
 */

/**
 * Omnitec functions and definitions
 *
 */


/* ---------- require classes ------ */
//setup class
require_once('libs/SetupClass.php');

//remove admin pages class
require_once('libs/RemoveAdminPagesClass.php');

//theme settings page class
require_once('libs/ThemeSettingsPageClass.php');

//mime type class
require_once('libs/MimeTypeClass.php');

//mime type class
require_once('libs/CrossDomainCookiesClass.php');

//tiles class
require_once('libs/TilesClass.php');



/* ---------- create objects and add actions ----- */
//lactoangin setup
$setup = new SetupClass();
add_action('init', array($setup, 'do_output_buffer'));
add_action('wp_enqueue_scripts', array($setup, 'register_styles'));
add_action('wp_enqueue_scripts', array($setup, 'register_scripts'));
add_filter( 'widget_title', array($setup, 'html_widget_title'));
$setup->loadTextdomain();
$setup->addThumbnails();
$setup->registerMenus();
$setup->registerSidebars();
$setup->addPostFormat();
$setup->addHTML5();
$setup->widgetDoShortcodes();
$setup->addImagesSizes();
$setup->disableAdminBar();


//remove admin pages action
$removeAdminPages = new RemoveAdminPagesClass();
add_action("admin_menu", array($removeAdminPages, "removePages"));


// add action to display theme options page
$addThemeSettignsPage = new ThemeSettingsPageClass();
add_action("admin_menu", array($addThemeSettignsPage, "addPageToMenu"));

//add cross domain cookies
$crossDomainCookies = new crossDomainCookiesClass();
add_action("init", array($crossDomainCookies, "init"));


//start session
if (!session_id()) {
    session_start();
}

// add action to display theme options page
$mimeTypes = new MimeTypeClass();
add_filter('upload_mimes', array($mimeTypes, 'custom_upload_mimes'));



//add tiles ajax support
$tiles = new TilesClass();
add_action("wp_ajax_load_items", array($tiles, 'ajaxLoadTiles'));
add_action("wp_ajax_nopriv_load_items", array($tiles, 'ajaxLoadTiles'));





//custom helpers
//format size units
function formatSizeUnits($bytes) {
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}

//slugify
function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    // trim
    $text = trim($text, '-');
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // lowercase
    $text = strtolower($text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}


function posts_by_year($post_type) {
    // array to use for results
    $years = array();

    // get posts from WP
    $posts = get_posts(array(
        'numberposts' => -1,
        'orderby' => 'post_date',
        'order' => 'ASC',
        'post_type' => $post_type,
        'post_status' => 'publish'
    ));

    // loop through posts, populating $years arrays
    foreach($posts as $post) {
        $years[date('Y', strtotime($post->post_date))][] = $post;
    }

    // reverse sort by year
    krsort($years);

    return $years;
}
?>