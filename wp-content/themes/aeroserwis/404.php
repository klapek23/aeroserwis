<?php get_header('subpage'); ?>

    <section class="banner">
        <img src="<?php echo get_template_directory_uri() . '/img/bkg/case-study-banner.jpg'; ?>" alt="banner">
    </section>

    <section class="single-page page-404">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="back">
                        <a href="<?php echo site_url(); ?>" title=""><span class="icon left-arrow-ico"></span><?php _e('Wróć do strony głównej', 'omnitec'); ?></a>
                    </div>
                    <article class="page-content" role="main">
                        <header>
                            <h1><?php _e('Błąd 404', 'omnitec'); ?></h1>
                        </header>
                        <section class="wysiwyg">
                            <p><?php _e('Upss.  Niestety, coś poszło nie tak. Zapraszamy do sprawdzenia innych podsron.', 'omnitec'); ?></p>
                        </section>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <nav class="nav-404">
                        <h2><?php _e('USŁUGI', 'omnitec'); ?></h2>
                        <ul>
                            <?php
                            $services = new WP_Query( array(
                                    'post_type' => 'services',
                                    'orderby' => 'date',
                                    'order'   => 'DESC',
                                    'post_parent' => 0
                                )
                            );

                            if ( $services->have_posts() ) {
                                while ( $services->have_posts() ) {
                                    $services->the_post();
                                    $services->post_date = strftime('%d %B %Y', strtotime($post->post_date));
                                    ?>
                                    <li>
                                        <a href="<?php echo get_permalink(); ?>" title="<?php echo $post->post_name; ?>"><?php echo strip_tags($post->post_title); ?> </a>
                                    </li>
                                <?php
                                }
                            } else {
                                echo 'no services found';
                            }
                            wp_reset_postdata();
                            ?>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-6">
                    <nav class="nav-404">
                        <h2><?php _e('CASE STUDY', 'omnitec'); ?></h2>
                        <ul>
                            <?php
                            $cases = new WP_Query( array(
                                    'post_type' => 'case-studies',
                                    'orderby' => 'date',
                                    'order'   => 'DESC',
                                    'post_parent' => 0
                                )
                            );

                            if ( $cases->have_posts() ) {
                                while ( $cases->have_posts() ) {
                                    $cases->the_post();
                                    $cases->post_date = strftime('%d %B %Y', strtotime($post->post_date));
                                    ?>
                                    <li>
                                        <a href="<?php echo get_permalink(); ?>" title="<?php echo $post->post_name; ?>"><?php echo strip_tags($post->post_title); ?> </a>
                                    </li>
                                <?php
                                }
                            } else {
                                echo 'no cases found';
                            }
                            wp_reset_postdata();
                            ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

<?php get_footer('subpage'); ?>