<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-11
 * Time: 16:10
 */
?>

<?php
//get data
$title = $section->post_title;
$form = array(
    'send_button' => get_field('send_button_text', $section->ID),
    'messages' => array(
        'required' => get_field('required_field_error_message', $section->ID),
        'email' => get_field('email_error_field_message', $section->ID),
        'thx' => get_field('thanks_message', $section->ID)
    )
);
$contact_section_title = get_field('content_title', $section->ID);
$contact_section_content = get_field('content', $section->ID);
?>

<section class="content-section contact" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $title; ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form id="contact-form">
                    <div class="message"><h3></h3></div>

                    <div class="form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-box text-input-box">
                                    <label for="name"><?php _e('Full name', 'klapek23_framework'); ?></label>
                                    <input type="text" id="name" name="name" placeholder="<?php _e('Enter full name', 'klapek23_framework'); ?>" value="">
                                </div>
                                <div class="input-box text-input-box">
                                    <label for="email"><?php _e('E-mail', 'klapek23_framework'); ?></label>
                                    <input type="email" id="email" name="email" placeholder="<?php _e('Enter e-mail', 'klapek23_framework'); ?>" value="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-box textarea-input-box">
                                    <label for="message"><?php _e('Message', 'klapek23_framework'); ?></label>
                                    <textarea id="message" name="message" placeholder="<?php _e('Enter message', 'klapek23_framework'); ?>" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="submit" class="button neutral-button"><em><?php echo $form['send_button']; ?></em></button>
                                    <input type="hidden" name="messages[required]" value="<?php echo $form['messages']['required']; ?>" >
                                    <input type="hidden" name="messages[email]" value="<?php echo $form['messages']['email']; ?>" >
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    (function() {
        //contact form validation
        $('#contact-form').find('button[type="submit"]').click( function(e) {
            var $form = $(this).closest('form'),
                $button = $(this),
                data = $form.serialize();
            $.ajax({
                url: '<?php echo get_template_directory_uri(); ?>/libs/contact-form-validation.php',
                data: data,
                type: 'post',
                dataType: 'json',
                success: function(reply) {
                    $form.find('input, textarea').removeClass('error')
                        .removeClass('ok');
                    $form.find('em.error').remove();

                    var status = reply.status,
                        errors = reply.errors;

                    if(status == false) {
                        $.each(errors, function(name, errors) {
                            $form.find('[name="'+name+'"]').addClass('error')
                                .after('<em class="error fadedOut">'+errors[0]+'</em>');

                            setTimeout( function() {
                                $form.find('em.error').removeClass('fadedOut');
                            }, 100);
                        });
                        $form.find('input:not(.error), textarea:not(.error)').addClass('ok');
                    } else {
                        $form.find('.form').fadeOut(300, function() {
                            $form.find('input, textarea').val('');
                            $form.find('.message').children('h3').text('<?php echo $form['messages']['thx']; ?>').addClass('positive');
                            $form.find('.message').fadeIn(400, function() {
                                setTimeout( function() {
                                    $form.find('.message').fadeOut(400, function() {
                                        $form.find('.message').children('h3').removeClass('positive').text('');
                                        $form.find('.form').fadeIn(300);
                                    });
                                }, 6000);
                            });
                        });
                    }
                }
            });
            e.preventDefault();
        });
    }(jQuery));
</script>