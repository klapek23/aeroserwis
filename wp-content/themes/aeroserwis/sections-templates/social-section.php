<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2016-01-05
 * Time: 00:10
 */

//get data
$title = $section->post_title;
$boxes = get_field('facebook_boxes', $section->ID);

?>

<section class="content-section social-section" >
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><?php echo $title; ?></h2>
                </div>
            </div>
            <div class="row">
                <?php foreach($boxes as $box): ?>
                <div class="col-md-6 col-sm-6">
                    <div class="box">
                        <h3><?php echo $box['title']; ?></h3>
                        <?php echo $box['facebook_script']; ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <div class="background" style="background-image: url(<?php the_field('background_image', $section->ID); ?>)"></div>
</section>