<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-02-03
 * Time: 22:39
 */

//get data
$title = $section->post_title;
$list = get_field('contact_items', $section->ID);
$marker = get_field('marker', $section->ID);
$markerTitle = get_field('marker_title', $section->ID);
$markerInfowindowContent = get_field('marker_infowindow_content', $section->ID);
$mapCenter = get_field('map_center', $section->ID);
?>

<section class="map" id="map">
    <div id="gmap"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-7 col-xxs-10">
                <article class="contactInfo">
                    <h3><?php echo $title; ?></h3>
                    <div class="table">
                        <?php foreach($list as $row): ?>
                        <div class="table-row">
                            <div class="cell"><?php echo $row['text']; ?></div>
                            <div class="cell ico">
                                <?php echo $row['icon']; ?>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

<script>
    //google map
    (function($) {
        var $gmap = $('#gmap');
        if($gmap.length > 0) {
            function mapInit() {
                var styles = [
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            { "color": "#e8e7e3" }
                        ]
                    },{
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            { "color": "#eef1ed" }
                        ]
                    },{
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            { "color": "#ffffff" }
                        ]
                    },{
                        "featureType": "all",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            { "color": "#9f9f9f" }
                        ]
                    },{
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            { "color": "#ffffff" }
                        ]
                    },{
                        "featureType": "road",
                        "elementType": "labels.icon",
                        "stylers": [
                            { "visibility": "off" }
                        ]
                    },{
                        "featureType": "poi",
                        "elementType": "labels.icon",
                        "stylers": [
                            { "visibility": "off" }
                        ]
                    },{
                        "featureType": "transit",
                        "elementType": "labels.icon",
                        "stylers": [
                            { "visibility": "off" }
                        ]
                    },{
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            { "color": "#cbdffc" }
                        ]
                    }
                ]

                var styledMap = new google.maps.StyledMapType(styles,
                    {name: "Styled Map"});

                var mapProp = {
                    center: new google.maps.LatLng(<?php echo $mapCenter; ?>),
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    panControl: false,
                    zoomControl: true,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    overviewMapControl: false,
                    scrollwheel: false
                };
                var map = new google.maps.Map(document.getElementById("gmap"), mapProp);

                map.mapTypes.set('map_style', styledMap);
                map.setMapTypeId('map_style');

                var infowindow = new google.maps.InfoWindow({
                    content: '<?php echo trim(preg_replace('/\s\s+/', ' ', nl2br($markerInfowindowContent))); ?>'
                });

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo $marker; ?>),
                    map: map,
                    title: '<?php echo trim(preg_replace('/\s\s+/', ' ', nl2br($markerTitle))); ?>',
                    icon: '/wp-content/themes/aeroserwis/img/ico/map-marker.png'
                });

                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            }
            google.maps.event.addDomListener(window, 'load', mapInit);
        }
    })(jQuery);
</script>