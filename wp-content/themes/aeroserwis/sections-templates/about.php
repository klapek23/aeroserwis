<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-06
 * Time: 22:53
 */
?>

<?php
//get data
$title = $section->post_title;
$content = $section->post_content;
?>

<section class="content-section about" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $title; ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <article class="wysiwyg">
                    <?php echo $content; ?>
                </article>
            </div>
        </div>
    </div>
</section>