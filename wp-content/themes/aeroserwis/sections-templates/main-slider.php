<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-06
 * Time: 22:53
 */
?>

<?php
//get data
$title = $section->post_title;
$content = $section->post_content;
$slides = get_field('slides', $section->ID);
?>

<?php if(!empty($slides)): ?>
    <div class="main-slider" id="top">
        <div class="slides slider-content">

            <?php foreach($slides as $slide):
                $subtitle = $slide['slide_subtitle'];
                $title = $slide['slide_title'];
                $text = $slide['slide_text'];
                $link = $slide['slide_link'];
                $image = $slide['slide_image'];
            ?>

            <div class="slide" style="background-image: url(<?php echo $image; ?>)"; >
                <div class="slide-overlay"></div>
                <div class="content">
                    <article>
                        <h2><?php echo $title; ?></h2>
                        <h3><?php echo $subtitle; ?></h3>
                        <p><?php echo $text; ?></p>
                        <a href="<?php echo $link; ?>" class="" title="<?php _e('View more', 'klapek23_framework'); ?>"><?php _e('View', 'klapek23_framework'); ?> <i class="icon icon_more_arrow"></i></a>
                    </article>
                </div>
            </div>
            <?php endforeach; ?>
        </div>

        <nav class="pager">
            <ul>
                <?php foreach($slides as $slide): ?>
                    <li><span><em></em></span></li>
                <?php endforeach; ?>
            </ul>
        </nav>
    </div>
<?php endif; ?>