<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-08
 * Time: 22:42
 */

//get data
$services = get_field('services', $section->ID);
?>

<section class="content-section services" id="services">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $section->post_title; ?></h2>
            </div>
        </div>
        <div class="row">
            <?php $i = 1;
            foreach($services as $service):
                $service = $service['service'];
            ?>
                <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12">
                    <article>
                        <a href="<?php echo get_permalink($service->ID); ?>" title="<?php echo $service->post_title; ?>">
                            <div class="icon">
                                <div class="table">
                                    <div class="center">
                                        <?php the_field('icon', $service->ID); ?>
                                    </div>
                                </div>
                            </div>
                            <h3><?php echo $service->post_title; ?></h3>
                        </a>
                    </article>
                </div>
                <?php echo ($i % 2 == 0 ? '<div class="clearfix hidden-sm hidden-md hidden-lg"></div>' : ''); ?>
                <?php $i++; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>