<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-06
 * Time: 22:11
 */
?>

<?php
//get data
$content = get_field('content', $section->ID);
$cookies_link_url = get_field('cookies_link', $section->ID);
$cookies_link_label= get_field('cookies_link_label', $section->ID);
?>

<?php if(!isset($_COOKIE['cookies-accept'])): ?>
    <section class="cookies">
        <div class="inside">
            <p><?php echo $content; ?></p>
            <span class="close-button"><a href="#" title="<?php _e('Close', 'klapek23_framework'); ?>"><i class="fa fa-times-circle"></i></a></span>
        </div>
    </section>
<?php endif; ?>