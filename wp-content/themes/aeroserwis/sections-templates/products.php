<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2015-01-08
 * Time: 22:42
 */

//get data
$products = get_field('products', $section->ID);
?>

<section class="content-section products" id="products">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $section->post_title; ?></h2>
            </div>
        </div>
        <div class="row">
            <?php
            $productID = 1;
            foreach($products as $product):
                $product = $product['product'];
                $thumbUrl = wp_get_attachment_url(get_post_thumbnail_id($product->ID));
                $intro = get_field('short_info', $product->ID);
                ?>
                <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 product">
                    <article>
                        <a href="<?php echo get_permalink(($product->ID)); ?>" title="<?php echo $product->post_title; ?>">
                            <div class="flip">
                                <div class="card">
                                    <div class="front">
                                        <img src="<?php echo $thumbUrl; ?>" class="img-responsive" alt="<?php echo $product->post_title; ?>">
                                    </div>
                                    <div class="back">
                                        <h4><?php echo $product->post_title; ?></h4>
                                        <p><?php echo $intro; ?></p>
                                    </div>
                                </div>
                            </div>
                            <h4><?php echo $product->post_title; ?></h4>
                        </a>
                    </article>
                </div>

                <?php if($productID % 3 == 0): ?>
                    <div class="clearfix hidden-xs hidden-sm"></div>
                <?php endif; ?>

                <?php if($productID % 2 == 0): ?>
                    <div class="clearfix hidden-md hidden-lg"></div>
                <?php endif; ?>
            <?php
            $productID++;
            endforeach;
            ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right show-all">
                    <a href="<?php the_field('show_all_link', $section->ID); ?>" class="button neutral-button" title="<?php the_field('show_all_text', $section->ID); ?>"><?php the_field('show_all_text', $section->ID); ?></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>