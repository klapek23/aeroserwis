<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aeroserwis');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',[yN^(z#[Al=BCRY4/%:. Z|!B2+RU|RT~W0E-zO&{~>WY)>5U#f[x]&ZP*k# , ');
define('SECURE_AUTH_KEY',  '>QX#9zTfhU}b~ea+*Z-|P5v-6-ZGFZk6 ArLM ={YF0OY.xZ/&jMQkaPN{_q_O>Z');
define('LOGGED_IN_KEY',    '@6HCU3h@sN%@!II<)voX&xk]TgT4DnwFCc]b2$R?/:<-uXXMo|CI@--|)HZSe]h7');
define('NONCE_KEY',        ':j]uu;>%|Vr2 [tq<(A?]Aptktt5cK3>Ys50?[@1w@@wK&r|d%>V`TsTM^.S18,?');
define('AUTH_SALT',        '?!$qN]t*hnkPw-pk(yy!X;6V<nW7Afm<4L9_ZuGk7s}PX|Uhx%bp|=s][PS1roS|');
define('SECURE_AUTH_SALT', 'L:|zcFDP8PDp|g3*w})+_(8Pi?LgF*w7RbHJ!7^N)3{XZ Bi2RtMaBO7%R77)g>Y');
define('LOGGED_IN_SALT',   ']|:)29as}#S6c)-_rY~^-s#Vu5kDhoooA+~|m|E#m6bw&L%~kW!vS|z|wQyn9lqq');
define('NONCE_SALT',       '^)o|EI[*7q>G94MM4aox9Eu|22_oMECz|v7ImrzlPn?+e-~y( @x ,i|UZ|t>EH(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

define('WPLANG', '');

//allow multisite
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
//define('DOMAIN_CURRENT_SITE', 'www.aeroserwis.localhost');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
